#define __SFR_OFFSET 0
#include <avr/io.h>
#include <avr/eeprom.h>

        .text

main:                       ;asd
        adc     r2, r5      ; 0
flag2:        add     r3, r16     ; 2
        adiw    r25:r24, 0x12   ; 4
        and     r4, r24     ; 6
        andi    r18, 0x55   ; 8 
        asr     r7          ; 10


        bclr    5           ; 12
        bset    7           ; 14
flag:   bld     r4, 4       ; 16
        brbc    1, mainn     ; 18
        brbs    2, mainn     ; 20
        brcc    mainn        ; 22
        brcs    mainn        ; 24
        break               ; 26
        breq    mainn        ; 28
        brge    main        ; 30
        brhc    main        ; 32
        brhs    main        ; 34
        brid    main        ; 36
        brie    flag2        ; 38
        brlo    main        ; 40
        brlt    main        ; 42
        brmi    main        ; 44
        brne    main        ; 46
        brpl    main        ; 48
        brsh    main        ; 50
        brtc    main        ; 52
        brts    main        ; 54
        brvc    main        ; 56
        brvs    main        ; 58
        bset    3           ; 60
        bst     r2, 3       ; 62

        call    main        ; 64
        
        cbi     15, 6       ; 68
        cbr     r25, 0xaa   ; 70
        clc                 ; 72
        clh                 ; 74
        cli                 ; 76
        cln                 ; 78
        clr     r6
        cls
        clt
        clv
        clz
        com     r13
        cp      r14, r15
        cpc     r14, r15
        cpi     r18, 45
        cpse    r3, r9

        dec     r14
        
        elpm
        elpm    R4, Z
        elpm    R21, Z+
        eor     r4, r14

        fmul    r16, r17
        fmuls   r16, r17
        fmulsu  r17, r18

        icall
        ijmp
        in      r13, 31
        inc     r27

        jmp     main

        ld      r5, X
        ld      r5, X+
        ld      r5, -X

        ld      r6, Y
        ld      r6, Y+
        ld      r6, -Y
        ldd     r6, Y+6

        ld      r6, Z
        ld      r6, Z+
        ld      r6, -Z
        ldd     r6, Z+6

        ldi     r16, 123
        lds     r17, 12345
        lpm
        lpm     r5, Z
        lpm     r4, Z+
        lsl     r7
        lsr     r8

        mov     r31, r1
        movw    r4, r8
        mul     r18, r19
        muls    r20, r21
        mulsu   r22, r23

        neg     r9
        nop

        or      r16, r17
        ori     r16, 0x80
        out     0x31, r8

        pop     r7
        push    r9

        rcall   main
        ret
        reti
        rjmp    main
        ror     r29
        rol     r30

        sbc     r4, r5
        sbci    r16, 34
        sbi     0x14, 6
        sbic    0x14, 5
        sbis    0x14, 3
        sbiw    YH:YL, 45
        sbr     r26, 0x44
        sbrs    r20, 3
        sec
        seh
        sei
        seh
        ;ser     r29
        ses
        set
        sev
        sez
        sleep
        spm     Z+
        st      X, r4
        st      X+, r4
        st      -X, R4
        st      Y, r4
        st      Y+, r4
        st      -Y, R4
        std     Y+4, R4
        st      Z, r4
        st      Z+, r4
        st      -Z, R4
        std     Z+4, R4
        ;sts     0xffff, r6 ; 0>65535
        sts     0x123, r8
        sub     r7, r8
        subi    r17, 123
        swap    r9

        tst     r11

        wdr

        sbiw    r30, 45
        sbiw    r31:r30, 45
        sbiw    YH:YL, 45
        
        adiw    r24, 0x12
        adiw    r25:r24, 0x12
        adiw    YH:YL, 0x12   ; modificado
        
        movw    r4, r8
        movw    r5:r4, r9:r8 ;modificado
        movw    YH:YL, zH:zL
        
        .end
