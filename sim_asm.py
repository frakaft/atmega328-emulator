#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  demo_pyparsing.py
#  
#  Copyright 2018 Unknown <root@hp425>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys

from pyparsing import Word, Literal, CaselessLiteral, White
from pyparsing import Combine, Optional, MatchFirst, SkipTo, LineEnd, ParseFatalException, ParseException, StringEnd
from pyparsing import alphas, nums, hexnums, oneOf, tokenMap

from Atmega328 import Atmega328
from sym_table import Symbol_table

class Assembler():
    def __init__(self, cpu):
        self.cpu = cpu
        self.txt = []
        self.opcode_dict = {}
        
        for nr, opcode in enumerate(self.cpu.asm_opcodes):
            self.opcode_dict[opcode.opcstr] = nr
            
        self.parser = self.bnf()
        self.parser_first = self.bnf_first()

    def get_txt (self):
        for line in self.txt:
            print(line)

    def bnf(self):
        hashtag = Literal("#")
        dot = Literal(".")
        colon = Literal(":")
        comma = Literal(',')
        plus = Literal('+')
        minus = Literal('-')
        less = Literal('<')
        greater = Literal('>')
        semicolon = Literal(';')
        hex_prefix = Literal('0x')
        oct_prefix = Literal('0o')
        bin_prefix = Literal('0b')
        reg = CaselessLiteral('r')
        
        reg_mx = CaselessLiteral('-X')
        reg_my = CaselessLiteral('-Y')
        reg_mz = CaselessLiteral('-Z')
        reg_xp = CaselessLiteral('X+')
        reg_yp = CaselessLiteral('Y+')
        reg_zp = CaselessLiteral('Z+')
        reg_x = CaselessLiteral('X')
        reg_y = CaselessLiteral('Y')
        reg_z = CaselessLiteral('Z')
        
        reg_xh = CaselessLiteral('XH').setParseAction(lambda toks: 27)
        reg_xl = CaselessLiteral('XL').setParseAction(lambda toks: 26)
        reg_yh = CaselessLiteral('YH').setParseAction(lambda toks: 29)
        reg_yl = CaselessLiteral('YL').setParseAction(lambda toks: 28)
        reg_zh = CaselessLiteral('ZH').setParseAction(lambda toks: 31)
        reg_zl = CaselessLiteral('ZL').setParseAction(lambda toks: 30)

        offset = Literal("__SFR_OFFSET")
        include = Literal('include')
        define = Literal('define')
        library = Word(alphas, alphas + nums + '_' + '.' + '/')
        identifier = Word(alphas, alphas + nums + '_')
        integer = Word(nums)
        include_tag = include +  less + library + greater
        define_tag = define + offset + integer
        direct = include_tag | define_tag
        hash_direc = hashtag + direct
        
        label = (identifier + colon).setParseAction(lambda toks: ['LABEL',toks[0]])
        reg_num = Word(nums)
        comment = semicolon + SkipTo(LineEnd())
        
        etiqueta = identifier.copy().setParseAction(lambda toks: self.cpu.symtable.find_symbol(toks[0])) # buscar etiqueta en symtable
        
        op_imm_hex = Combine(hex_prefix + Word(hexnums)).setParseAction(tokenMap(int,0))
        op_imm_oct = Combine(oct_prefix + Word(nums[:8])).setParseAction(tokenMap(int,0))
        op_imm_bin = Combine(bin_prefix + Word(nums[:2])).setParseAction(tokenMap(int,0))
        op_imm_dec = Word(nums).setParseAction(tokenMap(int))
        
        op_imm = op_imm_hex | op_imm_bin | op_imm_dec | op_imm_oct
        op_reg = reg.suppress() + reg_num.setParseAction(tokenMap(int))
        
        opcode_imm_4_list = oneOf('des').setParseAction(lambda toks: self.decode_remainder(toks[0],'imm_4'))
        opcode_bit_list = oneOf('bcrl bset bclr').setParseAction(lambda toks: self.decode_remainder(toks[0],'bit'))
        opcode_reg_list = oneOf('asr clr com dec inc lsl lsr neg pop push rol ror swap tst xch').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg'))
        opcode_reg_imm_list = oneOf('sbr').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_imm'))
        opcode_reg8_imm_list = oneOf('andi cpi ldi ori sbci subi cbr').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg8_imm'))
        opcode_reg_imm16_list = oneOf('lds').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_imm16'))
        opcode_imm16_reg_list = oneOf('sts').setParseAction(lambda toks: self.decode_remainder(toks[0],'imm16_reg'))
        opcode_imm7_reg_list = oneOf('sts').setParseAction(lambda toks: self.decode_remainder(toks[0],'imm7_reg'))
        opcode_reg_bit_list = oneOf('bld bst sbrc sbrs').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_bit'))
        #opcode_bit_reg_list = oneOf('')
        opcode_reg_reg_list = oneOf("adc add and cp cpc cpse eor mov mul or sbc sub").setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_reg'))
        opcode_reg3_reg3_list = oneOf('mulsu').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg3_reg3'))
        opcode_reg4_reg4_list = oneOf('muls').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg4_reg4'))
        opcode_reg4_list = oneOf('ser').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg4'))
        opcode_reg_x_list = oneOf('ld').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_x'))
        opcode_reg_mx_list = oneOf('ld').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_mx'))
        opcode_reg_xp_list = oneOf('ld').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_xp'))
        opcode_reg_y_list = oneOf('ld').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_y'))
        opcode_reg_yp_list = oneOf('ld').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_yp'))
        opcode_reg_my_list = oneOf('ld').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_my'))
        opcode_reg_yo_list = oneOf('ldd').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_yo'))
        opcode_reg_z_list = oneOf('ld lpm elpm').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_z'))
        opcode_reg_zp_list = oneOf('elpm ld lpm').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_zp'))
        opcode_reg_mz_list = oneOf('ld').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_mz'))
        opcode_reg_zo_list = oneOf('ldd').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_zo'))
        opcode_reg_io_list = oneOf('in').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg_io'))
        opcode_x_reg_list = oneOf('st').setParseAction(lambda toks: self.decode_remainder(toks[0],'x_reg'))
        opcode_xp_reg_list = oneOf('st').setParseAction(lambda toks: self.decode_remainder(toks[0],'xp_reg'))
        opcode_mx_reg_list = oneOf('st').setParseAction(lambda toks: self.decode_remainder(toks[0],'mx_reg'))
        opcode_y_reg_list = oneOf('st').setParseAction(lambda toks: self.decode_remainder(toks[0],'y_reg'))
        opcode_yp_reg_list = oneOf('st').setParseAction(lambda toks: self.decode_remainder(toks[0],'yp_reg'))
        opcode_my_reg_list = oneOf('st').setParseAction(lambda toks: self.decode_remainder(toks[0],'my_reg'))
        opcode_yo_reg_list = oneOf('std').setParseAction(lambda toks: self.decode_remainder(toks[0],'yo_reg'))
        opcode_z_reg_list = oneOf('st lac las lat').setParseAction(lambda toks: self.decode_remainder(toks[0],'z_reg'))
        opcode_zp_reg_list = oneOf('st').setParseAction(lambda toks: self.decode_remainder(toks[0],'zp_reg'))
        opcode_mz_reg_list = oneOf('st').setParseAction(lambda toks: self.decode_remainder(toks[0],'mz_reg'))
        opcode_zo_reg_list = oneOf('std').setParseAction(lambda toks: self.decode_remainder(toks[0],'zo_reg'))
        opcode_dreg_imm_list = oneOf('adiw sbiw').setParseAction(lambda toks: self.decode_remainder(toks[0],'dreg_imm'))
        opcode_dreg_dreg_list = oneOf('movw').setParseAction(lambda toks: self.decode_remainder(toks[0],'dreg_dreg'))
        opcode_rel_add_list = oneOf('brcc brcs breq brge brhc brhs brid brie brlo brlt brmi brne brpl brsh brtc brts brvc brvs').setParseAction(lambda toks: self.decode_remainder(toks[0],'rel_add'))
        opcode_rel_add12_list = oneOf('rcall rjmp').setParseAction(lambda toks: self.decode_remainder(toks[0],'rel_add12'))
        opcode_add17_list = oneOf('call jmp').setParseAction(lambda toks: self.decode_remainder(toks[0],'add17'))
        opcode_bit_rel_list = oneOf('brbc brbs').setParseAction(lambda toks: self.decode_remainder(toks[0],'bit_rel'))
        opcode_io_bit_list = oneOf('cbi sbi sbic sbis').setParseAction(lambda toks: self.decode_remainder(toks[0],'io_bit'))
        opcode_io_reg_list = oneOf('out').setParseAction(lambda toks: self.decode_remainder(toks[0],'io_reg'))
        opcode_reg8_reg8_list = oneOf('fmul fmuls fmulsu').setParseAction(lambda toks: self.decode_remainder(toks[0],'reg8_reg8'))
        opcode_just_zp_list = oneOf('spm').setParseAction(lambda toks: self.decode_remainder(toks[0],'just_zp'))
        opcode_no_opd_list = oneOf("break clc clh cli cln cls clt clv clz eicall eijmp elpm icall ijmp lpm nop ret reti sec seh sei sen ses set sev sez sleep spm wdr").setParseAction(lambda toks: self.decode_remainder(toks[0],'no_opd'))
        
        offset = op_imm.copy().addCondition(lambda toks: toks[0] <= 63, message='offset must be between 0 and 63')
        
        op_reg_0_31 = op_reg.copy().addCondition(lambda toks: toks[0] <= 31, message='register must be between 0 and 31')
        op_reg_16_31 = op_reg.copy().addCondition(lambda toks: toks[0] >= 16 and toks[0] <= 31, message='register must be between 16 and 31').setParseAction(lambda toks: toks[0]-16)
        op_reg_16_23 = op_reg.copy().addCondition(lambda toks: toks[0] >= 16 and toks[0] <= 23, message='register must be between 16 and 23').setParseAction(lambda toks: toks[0]-16)
        
        op_reg_24_30_pair = op_reg.copy().addCondition(lambda toks: toks[0] in range(24, 31, 2), message='register must be pair and between 24 and 30')
        op_reg_0_30_pair = op_reg.copy().addCondition(lambda toks: toks[0] in range(0, 31, 2), message='register must be pair and between 0 and 30')
        op_reg_24_30_odd = op_reg.copy().addCondition(lambda toks: toks[0] in range(25, 32, 2), message='register must be odd and between 25 and 31')
        op_reg_0_30_odd = op_reg.copy().addCondition(lambda toks: toks[0] in range(1, 32, 2), message='register must be odd and between 1 and 31')
        
        op_imm_0_255 = op_imm.copy().addCondition(lambda toks: toks[0] <= 255, message='immediate must be between 0 and 255')
        op_imm_00_0f = op_imm.copy().addCondition(lambda toks: toks[0] <= 0x0f, message='immediate must be between 0x00 and 0x0f')
        op_imm_0_7 = op_imm.copy().addCondition(lambda toks: toks[0] <= 7, message='immediate must be between 0 and 7')
        op_imm_0_65535 = op_imm.copy().addCondition(lambda toks: toks[0] <= 65535, message='immediate must be between 0 and 65535')
        op_imm_0_63 = op_imm.copy().addCondition(lambda toks: toks[0] <= 63, message='immediate must be between 0 and 63')
        op_imm_0_31 = op_imm.copy().addCondition(lambda toks: toks[0] <= 31, message='immediate must be between 0 and 31')
        op_imm_0_127 = op_imm.copy().addCondition(lambda toks: toks[0] <= 127, message='immediate must be between 0 and 127')
        
        op_reg_24_30_pair_w = (op_reg_24_30_odd + colon.suppress() + op_reg_24_30_pair).copy().addCondition(lambda toks: (toks[0] - 1) == toks[1], message='argument must be: Rd+1 : Rd').setParseAction(lambda toks: toks[1])
        op_reg_0_30_pair_w = (op_reg_0_30_odd + colon.suppress() + op_reg_0_30_pair).copy().addCondition(lambda toks: (toks[0] - 1) == toks[1], message='argument must be: Rd+1 : Rd').setParseAction(lambda toks: toks[1])
        
        op_reg_xhl = reg_xh + colon.suppress() + reg_xl
        op_reg_yhl = reg_yh + colon.suppress() + reg_yl
        op_reg_zhl = reg_zh + colon.suppress() + reg_zl
        
        op_reg_dreg_imm = (op_reg_24_30_pair_w | op_reg_xhl | op_reg_yhl | op_reg_zhl | op_reg_24_30_pair).copy().setParseAction(lambda toks: (toks[0]-24)//2)
        op_reg_dreg_dreg = (op_reg_0_30_pair_w | op_reg_xhl | op_reg_yhl | op_reg_zhl | op_reg_0_30_pair).copy().setParseAction(lambda toks: toks[0]//2)
        
        opcode_imm_4 = opcode_imm_4_list + op_imm_00_0f # immediato 00 -> 0f
        opcode_bit = opcode_bit_list + op_imm_0_7 # immediat 0 -> 7
        opcode_reg = opcode_reg_list + op_reg_0_31 # registro 0 -> 31
        opcode_reg_imm = opcode_reg_imm_list + op_reg_16_31 + comma.suppress() + op_imm_0_255 # registro 16 -> 31, immediato 0 -> 255
        opcode_reg8_imm = opcode_reg8_imm_list + op_reg_16_31 + comma.suppress() + op_imm_0_255 # registro 16 -> 31, immediato 0 -> 255
        opcode_reg_imm16 = opcode_reg_imm16_list + op_reg_0_31 + comma.suppress() + op_imm_0_65535 # registro 0 -> 31, immediato 0 > 65535
        opcode_imm16_reg = opcode_imm16_reg_list + op_imm_0_65535 + comma.suppress() + op_reg_0_31 # immediato 0->65535, registro 0-> 31
        opcode_imm7_reg = opcode_imm7_reg_list + op_imm_0_127 + comma.suppress() + op_reg_16_31 # immediato 0->127, registro 16-> 31
        opcode_reg_bit =  opcode_reg_bit_list + op_reg_0_31 + comma.suppress() + op_imm_0_7 # registro 0 -> 31, immediato 0 -> 7
        #opcode_bit_reg = opcode_reg_bit_list # no implemntado
        opcode_reg_reg = opcode_reg_reg_list + op_reg_0_31 + comma.suppress() + op_reg_0_31 # registro 0 -> 31, registro 0 -> 31
        opcode_reg3_reg3 = opcode_reg3_reg3_list + op_reg_16_23 + comma.suppress() + op_reg_16_23 # registro 16 -> 23, registro 16 -> 23
        opcode_reg4_reg4 = opcode_reg4_reg4_list + op_reg_16_31 + comma.suppress() + op_reg_16_31 # registro 16 -> 31, registro 16 -> 31
        opcode_reg4 = opcode_reg4_list + op_reg_16_31 # registro 16 -> 31
        
        # OPCODE registro 0 -> 31, REG
        opcode_reg_xp = opcode_reg_xp_list + op_reg_0_31 + comma.suppress() + reg_xp.suppress() # REG: X+
        opcode_reg_mx = opcode_reg_mx_list + op_reg_0_31 + comma.suppress() + reg_mx.suppress() # REG: -X
        opcode_reg_x = opcode_reg_x_list + op_reg_0_31 + comma.suppress() + reg_x.suppress() # REG: X
        opcode_reg_yp = opcode_reg_yp_list + op_reg_0_31 + comma.suppress() + reg_yp.suppress() # REG: Y+
        opcode_reg_my = opcode_reg_my_list + op_reg_0_31 + comma.suppress() + reg_my.suppress() # REG: -Y
        opcode_reg_y = opcode_reg_y_list + op_reg_0_31 + comma.suppress() + reg_y.suppress() # REG: Y 
        opcode_reg_yo = opcode_reg_yo_list + op_reg_0_31 + comma.suppress() + reg_y.suppress() + plus.suppress() + offset # REG: Y + q 0 -> 63
        opcode_reg_zp = opcode_reg_zp_list + op_reg_0_31 + comma.suppress() + reg_zp.suppress() # REG: Z+
        opcode_reg_mz = opcode_reg_mz_list + op_reg_0_31 + comma.suppress() + reg_mz.suppress() # REG: -z
        opcode_reg_z = opcode_reg_z_list + op_reg_0_31 + comma.suppress() + reg_z.suppress() # REG: Z
        opcode_reg_zo = opcode_reg_zo_list + op_reg_0_31 + comma.suppress() + reg_z.suppress() + plus.suppress() + offset # REG: Z + q 0 -> 31
        
        # OPCODE REG, registro 0 -> 31
        opcode_x_reg = opcode_x_reg_list + reg_x.suppress() + comma.suppress() + op_reg_0_31  # REG: X
        opcode_xp_reg = opcode_xp_reg_list + reg_xp.suppress() + comma.suppress() + op_reg_0_31  # REG: X+
        opcode_mx_reg = opcode_mx_reg_list + reg_mx.suppress() + comma.suppress() + op_reg_0_31  # REG: -X
        opcode_y_reg = opcode_y_reg_list + reg_y.suppress() + comma.suppress() + op_reg_0_31 # REG: Y
        opcode_yp_reg = opcode_yp_reg_list + reg_yp.suppress() + comma.suppress() + op_reg_0_31 # REG: Y+
        opcode_my_reg = opcode_my_reg_list + reg_my.suppress() + comma.suppress() + op_reg_0_31 # REG: -Y
        opcode_yo_reg = opcode_yo_reg_list + reg_y.suppress() + plus.suppress() + offset + comma.suppress() + op_reg_0_31 # REG: Y + q 0 -> 63
        opcode_z_reg = opcode_z_reg_list + reg_z.suppress() + comma.suppress() + op_reg_0_31 # REG: Z
        opcode_zp_reg = opcode_zp_reg_list + reg_zp.suppress() + comma.suppress() + op_reg_0_31 # REG: Z+
        opcode_mz_reg = opcode_mz_reg_list + reg_mz.suppress() + comma.suppress() + op_reg_0_31 # REG: -Z
        opcode_zo_reg = opcode_zo_reg_list + reg_z.suppress() + plus.suppress() + offset + comma.suppress() + op_reg_0_31 # REG: Z + q 0 -> 63
        
        # 32 bit operations
        opcode_dreg_imm = opcode_dreg_imm_list + op_reg_dreg_imm + comma.suppress() + op_imm_0_63 # Rd+1:Rd d∈[24,26,28,30], immediato 0 -> 63
        
        # 32 bit operations
        opcode_dreg_dreg = opcode_dreg_dreg_list + op_reg_dreg_dreg + comma.suppress() + op_reg_dreg_dreg # Rd+1:Rd , Rr+1:Rr r,d∈[0, 2, ... , 30]
        
        opcode_reg_io = opcode_reg_io_list + op_reg_0_31 + comma.suppress() + op_imm_0_63 # registro 0 -> 31, immediato 0 -> 63
        
        # OPCODE CON ETIQUETAS
        opcode_rel_add = opcode_rel_add_list + etiqueta # immediato -64 -> 64
        opcode_rel_add12 = opcode_rel_add12_list + etiqueta # immediato -2K -> 2K
        opcode_add17 = opcode_add17_list + etiqueta # immediato 0 -> 64K
        
        opcode_bit_rel = opcode_bit_rel_list + op_imm_0_7 + comma.suppress() + etiqueta # immediato 0 -> 7, immediato -64 -> 63
        opcode_io_bit = opcode_io_bit_list + op_imm_0_31 + comma.suppress() + op_imm_0_7 # immediato 0 -> 31, immediato 0 -> 7
        opcode_io_reg = opcode_io_reg_list + op_imm_0_63 + comma.suppress() + op_reg_0_31 # immediato 0 -> 63, registro 0 ->31
        opcode_reg8_reg8 = opcode_reg8_reg8_list + op_reg_16_23 + comma.suppress() + op_reg_16_23 # registro 16 -> 23, resgitro 16 -> 23
        opcode_just_zp = opcode_just_zp_list + reg_zp.suppress() # REG Z+
        opcode_no_opd = opcode_no_opd_list # NONE
        
        #set_op(x[?], msb, lsb, shift))
        # x[0] -> instruccion
        # x[1] -> primer operando
        # x[2] -> segundo operando
        
        opcode = (
                opcode_imm_4.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_bit.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_reg.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_reg_imm.setParseAction(lambda toks: toks[0] + ((toks[2]>>4)<<4) + (toks[1]<<4) + (toks[2]&0b1111) ) | #
                opcode_reg8_imm.setParseAction(lambda toks: toks[0] + ((toks[2]>>4)<<4) + (toks[1]<<4) + (toks[2]&0b1111) ) | #
                opcode_reg_imm16.setParseAction(lambda toks: [ toks[0] + (toks[1]<<4) , toks[2] ]) | #
                opcode_imm16_reg.setParseAction(lambda toks: [ toks[0] + (toks[1]<<4) , toks[2] ]) | #
                opcode_imm7_reg.setParseAction(lambda toks: toks[0] + ((toks[1]>>4)<<8) + (toks[2]<<4) + (toks[1]&0b1111) ) | #
                opcode_reg_bit.setParseAction(lambda toks: toks[0] + (toks[1]<<4) + (toks[2]) ) | #
                #opcode_bit_reg |
                opcode_reg_reg.setParseAction(lambda toks: toks[0] + ((toks[2]>>4)<<9) + (toks[1]<<1) + (toks[1]&0b1111) ) | #
                opcode_reg3_reg3.setParseAction(lambda toks: toks[0] + (toks[1]<<4) + (toks[2]) ) | #
                opcode_reg4_reg4.setParseAction(lambda toks: toks[0] + (toks[1]<<4) + (toks[2]) ) | #
                opcode_reg4.setParseAction(lambda toks: toks[0] + toks[1]<<4 ) | #
                opcode_reg_xp.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_reg_mx.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_reg_x.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_reg_yp.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_reg_my.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_reg_y.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_reg_yo.setParseAction(lambda toks: toks[0] + ((toks[2]>>5)<<13) + (((toks[2]>>3)&0b11)<<10) + (toks[1]<<4) + (toks[2]&0b111) ) | #
                opcode_reg_zp.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_reg_mz.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_reg_z.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_reg_zo.setParseAction(lambda toks: toks[0] + ((toks[2]>>5)<<13) | (((toks[2]>>3)&0b11)<<10) | (toks[1]<<4) | (toks[2]&0b111) ) | #
                opcode_x_reg.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_xp_reg.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_mx_reg.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_y_reg.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_yp_reg.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_my_reg.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_yo_reg.setParseAction(lambda toks: toks[0] + ((toks[1]>>5)<<13) + (((toks[1]>>3)&0b11)<<10) + (toks[2]<<4) + (toks[1]&0b111) ) | #
                opcode_z_reg.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_zp_reg.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_mz_reg.setParseAction(lambda toks: toks[0] + (toks[1] << 4)) | #
                opcode_zo_reg.setParseAction(lambda toks: toks[0] + ((toks[1]>>5)<<13) + (((toks[1]>>3)&0b11)<<10) + (toks[2]<<4) + (toks[1]&0b111) ) | #
                opcode_dreg_imm.setParseAction(lambda toks: toks[0] + ((toks[2]>>4)<<6) + (toks[1]<<4) + (toks[2]&0b1111) ) | #
                opcode_dreg_dreg.setParseAction(lambda toks: toks[0] + (toks[1]<<4) + (toks[2]) ) | #
                opcode_reg_io.setParseAction(lambda toks: toks[0] | ((toks[2]>>4)<<9) + (toks[1]<<4) + (toks[2]&0b1111) ) | #
                opcode_rel_add.setParseAction(lambda toks: toks[0] + (toks[1]<<3) ) | #
                opcode_rel_add12.setParseAction(lambda toks: toks[0] + toks[1] ) | #
                opcode_add17.setParseAction(lambda toks: [ toks[0] + ((toks[1]>>17)<<4) + ((toks[1]>>16)&0b1) , toks[1] & 0xffff ]) | #
                opcode_bit_rel.setParseAction(lambda toks: toks[0] + (toks[2]<<3) + (toks[1]) ) | #
                opcode_io_bit.setParseAction(lambda toks: toks[0] + (toks[1]<<3) + (toks[2]) ) | #
                opcode_io_reg.setParseAction(lambda toks: toks[0] + ((toks[1]>>4)<<9) + (toks[2]<<4) + (toks[1]&0b1111) ) | #
                opcode_reg8_reg8.setParseAction(lambda toks: toks[0] + (toks[1]<<4) + (toks[2]) ) | #
                opcode_just_zp | # SUPPRESS
                opcode_no_opd ) # SIN OPERADORES
        
        dot_direc = dot + identifier
        
        directive = hash_direc | dot_direc
        
        source = Optional(label.suppress()) + opcode
        
        line = ( directive.suppress() | source | label.suppress() | comment.suppress()) + Optional(comment.suppress()) #+ StringEnd()
        
        return line
    
    def bnf_first(self):
        hashtag = Literal("#")
        dot = Literal(".")
        colon = Literal(":")
        comma = Literal(',')
        plus = Literal('+')
        minus = Literal('-')
        less = Literal('<')
        greater = Literal('>')
        semicolon = Literal(';')
        hex_prefix = Literal('0x')
        oct_prefix = Literal('0o')
        bin_prefix = Literal('0b')
        reg_x = CaselessLiteral('x')
        reg_y = CaselessLiteral('y')
        reg_z = CaselessLiteral('z')
        reg = CaselessLiteral('r')
        high = CaselessLiteral('h')
        low = CaselessLiteral('l')
        
        reg_xyz = (reg_x | reg_y | reg_z) + Optional(plus | minus | high | low)
        
        offset = Literal("__SFR_OFFSET")
        include = Literal('include')
        define = Literal('define')
        library = Word(alphas, alphas + nums + '_' + '.' + '/')
        identifier = Word(alphas, alphas + nums + '_')
        integer = Word(nums)
        include_tag = include +  less + library + greater
        define_tag = define + offset + integer
        direct = include_tag | define_tag
        hash_direc = hashtag + direct
        
        label = (identifier + colon).setParseAction(lambda toks: ['LABEL',toks[0]])
        reg_num = Word(nums)
        comment = semicolon + SkipTo(LineEnd())
        
        op_imm_hex = Combine(hex_prefix + Word(hexnums))
        op_imm_oct = Combine(oct_prefix + Word(nums[:8]))
        op_imm_bin = Combine(bin_prefix + Word(nums[:2]))
        op_imm_dec = Word(nums)
        op_imm = op_imm_hex | op_imm_bin | op_imm_dec | op_imm_oct
        op_reg = reg + reg_num
        op_reg_w = op_reg + colon + op_reg
        reg_xyz_w = reg_xyz + colon + reg_xyz
        op = op_imm | op_reg_w | op_reg | identifier | reg_xyz_w | reg_xyz
        opcodes = oneOf('adc add adiw and andi asr bclr bcrl bld brbc brbs brcc brcs break breq brge brhc brhs brid brie brlo brlt brmi brne brpl brsh brtc brts brvc brvs bset bst call cbi cbr clc clh cli cln clr cls clt clv clz com cp cpc cpi cpse dec des eicall eijmp elpm eor fmul fmuls fmulsu icall ijmp in inc jmp lac las lat ld ldd ldi lds lpm lsl lsr mov movw mul muls mulsu neg nop or ori out pop push rcall ret reti rjmp rol ror sbc sbci sbi sbic sbis sbiw sbr sbrc sbrs sec seh sei sen ser ses set sev sez sleep spm st std sts sub subi swap tst wdr xch')
        
        opcode = opcodes + Optional(op).suppress() + Optional(comma + op).suppress()
        
        dot_direc = dot + identifier
        
        directive = hash_direc | dot_direc
        
        source = Optional(label) + opcode.setParseAction(lambda toks: self.set_16_32(toks[0]))
        
        line = ( directive.suppress() | source | label | comment.suppress()) + Optional(comment.suppress()) #+ StringEnd()
        
        return line
    
    def decode_remainder(self, op_code, op_type):
        
        special_op = {
            "elpm":{"no_opd":0x95d8,
                "reg_z":0x9006,
                "reg_zp":0x9007},

            "ld":{"reg_x":0x900c,
                "reg_xp":0x900d,
                "reg_mx":0x900e,
                "reg_y":0x8008,
                "reg_yp":0x9009,
                "reg_my":0x900a,
                "reg_z":0x8000,
                "reg_zp":0x9001,
                "reg_mz":0x9002},
            
            "ldd":{"reg_yo":0x8008,
                "reg_zo":0x8000},
            
            "lpm":{"no_opd":0x95c8,
                "reg_z":0x9004,
                "reg_zp":0x9005},
            
            "spm":{"no_opd":0x95e8,
                  "just_zp":0x95f8},
            
            "st":{"x_reg":0x920c,
                "xp_reg":0x920d,
                "mx_reg":0x920e,
                "y_reg":0x8208,
                "yp_reg":0x9209,
                "my_reg":0x920a,
                "z_reg":0x8200,
                "zp_reg":0x9201,
                "mz_reg":0x9202},
            
            "std":{"yo_reg":0x8208,
                "zo_reg":0x8200},
            
            "sts":{"imm16_reg":0x9200,
                    "imm7_reg":0xa800}
            }
        if op_code in special_op:
            return special_op[op_code][op_type]
        else:
            return self.cpu.asm_opcodes[self.opcode_dict[op_code]].remainder
        
    def set_16_32(self, op_code):
        list_32 = ['call', 'jmp', 'sts', 'lds']
        if op_code in list_32:
            return 4
        else:
            return 2
    
    def asm_one_instruction(self, line, parser):
        try:
            lista = parser.parseString(line)
        except (ParseFatalException, ParseException) as e:
            print(e.msg)
            sys.exit(1)
        return lista
        
        
    def asm_file_debug(self, input_file_name):
        try:
            input_file = open(input_file_name, 'r')
            for line in input_file: 
                if line.rstrip() == '':
                    continue
                print("LINEA {%s}"%(line.rstrip()))
                asm_list = self.asm_one_instruction(line.rstrip(), self.parser_first)
                print("INSTRUCCION %s \n"%(asm_list))
            input_file.close()
        except Exception as e:
            print(e)
            sys.exit(2)

    def set_op(self, num, msb, lsb, shift):
        return ((op & ((1<<msb+1)-1)) >> lsb) << shift
        
    def assemble(self):
        
        pc = 0
        index = 0;
        bitmap = [0]
        
        #pasada 1
        for line in self.txt:
            
            if bitmap[index] != pc :
                    bitmap.append(pc)
                    index += 1
            
            asm_line = self.asm_one_instruction(line, self.parser_first)
            if len(asm_line) != 0:
                print("0x%x    %s"%(bitmap[index], line))
            
            if len(asm_line) == 0 :
                continue
            elif len(asm_line) == 1 :
                pc += asm_line[0]
            elif len(asm_line) == 2 :
                if self.cpu.symtable.add(asm_line[1],bitmap[index]) == False :
                    print('etiqueta existente')
                    sys.exit(3)
            elif len(asm_line) == 3 :
                if self.cpu.symtable.add(asm_line[1],bitmap[index]) == False :
                    print('etiqueta existente')
                    sys.exit(3)
                pc += asm_line[2]
            else:
                print('error de sintaxis')
                sys.exit(4)
        
        #pasada 2
        index = 0
        for line in self.txt:
            print(line)
            asm_line = self.asm_one_instruction(line, self.parser)
            print(asm_line)
            if len(asm_line) == 0:
                continue
            elif len(asm_line) in (1,2,3) :
                if(bitmap[index +1] - bitmap[index]) > 2 :
                    self.cpu.flash.save_word(bitmap[index], asm_line[0])
                    self.cpu.flash.save_word(bitmap[index]+2, asm_line[1])
                    index+=1
                else:
                    self.cpu.flash.save_word(bitmap[index], asm_line[0])
                    index+=1
            else:
                print('error de sintaxiss')
                sys.exit(4)
        #print(self.cpu.flash.dump())
        
    def load_asm_file(self, file_name):
        self.txt = []
        try:
            input_file = open(file_name, 'r')
            for line in input_file:
                line = line.rstrip('\n ')
                if line != '':
                    self.txt.append(line)
            input_file.close()
        except Exception as e:
            print(e)
            sys.exit(2)
        
def main(args):
    
    symtable = Symbol_table()
    #symtable.add("main", 1234)
    cpu = Atmega328(symtable)
    asm = Assembler(cpu)
    #print(cpu.symtable.dump_table())
    #asm.asm_file_debug('validate_hex.S')
    #print(cpu.symtable.dump_table())
    
    #symtable.add("main", 1234)
    
    #asm.asm_file_debug('validate_hex.S')
    #print(cpu.symtable.dump_table())
    
    asm.load_asm_file('validate_hex.S')
    asm.assemble()
    print(cpu.symtable.dump_table())
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
